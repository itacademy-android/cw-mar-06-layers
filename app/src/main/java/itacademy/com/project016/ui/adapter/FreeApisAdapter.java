package itacademy.com.project016.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import itacademy.com.project016.R;
import itacademy.com.project016.data.dto.FreeApisModel;

public class FreeApisAdapter extends ArrayAdapter<FreeApisModel> {

    public FreeApisAdapter(@NonNull Context context, ArrayList<FreeApisModel> apisList) {
        super(context, 0, apisList);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_free_api, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        FreeApisModel model = getItem(position);
        if (model != null) {
            holder.tvDescription.setText(model.getDescription());
            holder.tvLink.setText(model.getLink());
            holder.cbProtected.setChecked(model.isHTTPS());
        }

        return convertView;
    }

    private class ViewHolder {
        private TextView tvDescription, tvLink;
        private CheckBox cbProtected;

        public ViewHolder(View view) {
            tvDescription = view.findViewById(R.id.tvDescription);
            tvLink = view.findViewById(R.id.tvLink);
            cbProtected = view.findViewById(R.id.cbProtected);
        }

    }
}
