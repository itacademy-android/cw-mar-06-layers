package itacademy.com.project016.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import itacademy.com.project016.data.dto.FreeApisModel;

public class ReadData {

    public ArrayList<FreeApisModel> apisList = new ArrayList<>();

    public void formatJSON(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("entries");

            for (int i = 0; i < 20; i++) {
                JSONObject apiObject = jsonArray.getJSONObject(i);
                FreeApisModel model = new FreeApisModel();
                model.setName(apiObject.getString("API"));
                model.setDescription(apiObject.getString("Description"));
                model.setLink(apiObject.getString("Link"));
                model.setCategory(apiObject.getString("Category"));
                model.setHTTPS(apiObject.getBoolean("HTTPS"));
                apisList.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
